import React, { Component } from 'react'
import {} from 'react-router-dom'
import Axios from 'axios';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom';
import Menu from './components/Menu';
import Home from './components/Home';
import AddNewArticle from './components/AddNewArticle';
import View from './components/View';

export default class App extends Component {
  constructor(){
    super();
    this.state={
      show:true,
      datas:[],
          
          TITLE:'',
          DESCRIPTION:'',
          IMAGE:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAMFBMVEXp7vG6vsG3u77s8fTCxsnn7O/f5OfFyczP09bM0dO8wMPk6ezY3eDd4uXR1tnJzdBvAX/cAAACVElEQVR4nO3b23KDIBRA0ShGU0n0//+2KmO94gWZ8Zxmr7fmwWEHJsJUHw8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwO1MHHdn+L3rIoK6eshsNJ8kTaJI07fERPOO1Nc1vgQm2oiBTWJ+d8+CqV1heplLzMRNonED+4mg7L6p591FC+133/xCRNCtd3nL9BlxWP++MOaXFdEXFjZ7r8D9l45C8y6aG0cWtP/SUGhs2d8dA/ZfGgrzYX+TVqcTNRRO9l+fS5eSYzQs85psUcuzk6igcLoHPz2J8gvzWaH/JLS+95RfOD8o1p5CU5R7l5LkfKEp0mQ1UX7hsVXqDpRrifILD/3S9CfmlUQFhQfuFu0STTyJ8gsP3PH7GVxN1FC4t2sbBy4TNRTu7LyHJbqaqKFw+/Q0ncFloo7CjRPwMnCWqKXQZ75El4nKC9dmcJaou9AXOE5UXbi+RGeJygrz8Uf+GewSn9uXuplnWDZJ7d8f24F/s6iq0LYf9olbS3Q8i5oKrRu4S9ybwaQ/aCkqtP3I28QDgeoK7TBya/aXqL5COx67PTCD2grtdOwH+pQV2r0a7YVBgZoKwwIVFQYG6ikMDVRTGByopjD8ATcKb0UhhRTe77sKs2DV7FKSjId18TUEBYVyLhUThWfILHTDqmI85/2RWWjcE/bhP6OD7maT3h20MHsA47JC3PsW0wcwLhv9t0OOPOIkCn21y2bXXwlyylxiYMPk1SuCSmpfK8bNQvIrpAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwNX4BCbAju9/X67UAAAAASUVORK5CYII=',
          checkTitle:false,
          checkDescription:false,
          check:false,
          checkTitleError:'',
          checkDescriptionError:'',
        }
  }


  componentDidMount(){
    Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
    .then(res=>{
      this.setState({
        datas:res.data.DATA,
        show:false,
      })
      console.log(this.state.datas)
    }).catch(err=>{
      console.log(err);
    })
  }

  handleChange=(e)=>{
    this.setState({
      [e.target.name]:e.target.value,
    })
    
  }

  //Add new item
  addValue=()=>{
  
    if(this.state.title===''){
      this.setState({
        checkTitleError:'*Please input your title.'
      })
    }
    if(this.state.description===''){
      this.setState({
        checkDescriptionError:'* Please input your description.'
      })
    }
    if(this.state.title!==''&&this.state.description!==''){

      Axios.post("http://110.74.194.124:15011/v1/api/articles",{
        "TITLE":this.state.title,
        "DESCRIPTION":this.state.description,
        "IMAGE":this.state.IMAGE,
      }).then(()=>{
        return Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
                .then(res=>{
                  this.setState({
                    datas:'',
                    datas:res.data.DATA,
                  })
                })
      }).catch(err=>{
        console.log(err);
      })
    }
    this.setState({
      
    })
  }

  //delete
  deleteItem=(id)=>{
    Axios.delete(`http://110.74.194.124:15011/v1/api/articles/${id}`)
    .then(() => {

      // Issue GET request after item deleted to get updated list
      // that excludes user of id
      return Axios.get(`http://110.74.194.124:15011/v1/api/articles?page=1&limit=15`)
      .then(res=>{
        this.setState({
          datas:'',
          datas:res.data.DATA,
        })
      })
      
  })
    console.log(id);
  }
  render() {
    if(this.state.show)
      return <h1>Loading</h1>
    return (
      <div>
        <Router>
          <Menu />
          <Switch>
            <Route path="/" exact render={()=><Home datas={this.state.datas} delete={this.deleteItem}/>}/>
            <Route path="/addnewarticle" render={()=><AddNewArticle 
            onChanged={this.handleChange} addValue={this.addValue} image={this.state.IMAGE}/>}/>
            <Route path="/View/:id" render={(props)=><View {...props} datas={this.state.datas}/>}/>
          </Switch>
        </Router>
      </div>
    )
  }
}
