import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Nav,Navbar,FormControl,Button,Form} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Menu() {
    return (
        <div>
            <Navbar bg="light" expand="lg">
                <div className="container">
                <Navbar.Brand href="#home">AMS</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                    <Nav.Link as={Link} to='/'>Home</Nav.Link>
                    </Nav>
                    <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
                </div>
            </Navbar>
        </div>
    )
}
