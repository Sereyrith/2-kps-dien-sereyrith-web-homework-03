import React from 'react'
import { Form, FormControl, Button } from 'react-bootstrap';
import '../App.css';
import { Link } from 'react-router-dom';

export default function AddNewArticle(props) {
    
    return (
        <div className="container"><br/>
            <div className="row">
                <div className="col-md-9">
                    <Form>
                        <Form.Group>
                            <Form.Label style={{fontFamily:'laila,serif'}}>TITLE</Form.Label>
                            <FormControl
                                type="text"
                                name="title"
                                onChange={(e)=>props.onChanged(e)}
                                value={props.title}
                                placeholder="Enter title" />
                            <div 
                                className={props.checkTitle ? 
                                "d-block":"d-none"} 
                                style={{marginLeft:"13px",fontSize:"16px",color:"red"}}>
                                &#x00021;{props.addValue.checkTitleError}
                            </div>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label style={{fontFamily:'laila,serif'}}>Description</Form.Label>
                            <FormControl 
                                type="text"
                                name="description"
                                onChange={(e)=>props.onChanged(e)}
                                value={props.description}
                                placeholder="Enter description"/>
                            <div 
                                className={props.checkDesription ? 
                                "d-block":"d-none"} 
                                style={{marginLeft:"13px",fontSize:"16px",color:"red"}}>
                                &#x00021;{props.addValue.checkDescriptionError}
                            </div>
                        </Form.Group>
                        <Link to={`/`}>
                            <Button variant="primary" onClick={()=>props.addValue()}>submit</Button>
                        </Link>
                    </Form>
                </div>
                <div className="col-md-3 img">
                    <img src={props.image}/>
                </div>
            </div>
        </div>
    )
}
