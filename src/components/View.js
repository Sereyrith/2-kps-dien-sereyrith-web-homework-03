import React from 'react'

export default function View({match, datas}) {
    var viewData = datas.find((d)=>d.ID==match.params.ID);
    return (
        <div>
            <h1>{viewData.TITLE}</h1>
            <img src={viewData.IMAGE}/>
            <h3>Description</h3>
            <p>{viewData.DESCRIPTION}</p>
        </div>
    )
}
