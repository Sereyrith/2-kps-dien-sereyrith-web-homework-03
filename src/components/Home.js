import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Table, Button} from 'react-bootstrap';
import {Link } from 'react-router-dom';
import '../App.css';

export default function Home(props) {
    let data = props.datas.map((d)=>
    <tr key={d.ID}>
        <td>{d.ID}</td>
        <td className="title">{d.TITLE}</td>
        <td className="description">{d.DESCRIPTION}</td>
        <td>{d.CREATED_DATE}</td>
        <td><img src={d.IMAGE} style={{height:'20px'}}/></td>
        <td>
            <Link><Button variant="primary" to={`/View/${d.ID}`}>View</Button> </Link>
             <Link> <Button variant="warning">Edit</Button> </Link> 
             <Button variant="danger" onClick={()=>props.delete(d.ID)}>Delete</Button></td>
    </tr>
    )
    return (
        <div className="container">
            <div className="App">
                <h1>Article Management</h1>
                <Link to={`/AddNewArticle`}><Button variant="secondary">Add New Article</Button></Link>
            </div><br/>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>TITLE</th>
                        <th>DESCRIPTION</th>
                        <th>CREATE DATE</th>
                        <th>IMAGE</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    {data}
                </tbody>
            </Table>
        </div>
    )
}
